FROM node:10

RUN dpkg --add-architecture i386 && apt-get update && apt-get install wine wine32 -y

RUN npm install electron-packager -g

VOLUME /electron
WORKDIR /electron